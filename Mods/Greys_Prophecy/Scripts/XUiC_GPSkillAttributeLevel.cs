﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class XUiC_GPSkillAttributeLevel : XUiC_SkillAttributeLevel
{
    public string[] DisabledFor { get; set; }

    public override void Init()
    {
        base.Init();
    }

    public override bool ParseAttribute(string _name, string _value, XUiController _parent)
    {
        var xmlAttribute = base.ParseAttribute(_name, _value, _parent);
        if (!xmlAttribute)
        {
            if(_name.ToLower().Equals("disabledfor") && !string.IsNullOrWhiteSpace(_value))
            {
                DisabledFor = _value.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                return true;
            }
        }

        return xmlAttribute;
    }
}
