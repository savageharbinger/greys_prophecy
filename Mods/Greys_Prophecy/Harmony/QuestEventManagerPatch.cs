﻿using DMT;
using Harmony;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using UnityEngine;

public class QuestEventManagerPatch : IHarmony
{
    public void Start()
    {
        var patchName = GetType().ToString();

        Debug.Log(" Loading Patch: " + patchName);
        var harmony = HarmonyInstance.Create(patchName);
        harmony.PatchAll(Assembly.GetExecutingAssembly());
    }

    [HarmonyPatch(typeof(QuestEventManager), "EntityKilled")]
    private class QuestEventManagerPatch_EntityKilled
    {
        public static bool Prefix(QuestEventManager __instance, Entity entity)
        {
            if(entity is EntityVulture)
            {
                __instance.ZombieKilled(EntityClass.list[entity.entityClass].entityClassName);
                return false;
            }

            return true;
        }
    }
}
