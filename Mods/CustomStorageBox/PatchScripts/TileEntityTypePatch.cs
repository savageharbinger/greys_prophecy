﻿using System;
using SDX.Compiler;
using Mono.Cecil;
using Mono.Cecil.Cil;
using System.Linq;
using DMT;

public class TileEntityTypePatch : IPatcherMod
{
    public bool Patch(ModuleDefinition module)
    {
        AddEnumOption(module, "TileEntityType", "EditableStorageBox", 22);

        return true;
    }

    private void AddEnumOption(ModuleDefinition gameModule, string enumName, string enumFieldName, byte enumValue)
    {
        var enumType = gameModule.Types.First(d => d.Name == enumName);
        FieldDefinition literal = new FieldDefinition(enumFieldName, FieldAttributes.Public | FieldAttributes.Static | FieldAttributes.Literal | FieldAttributes.HasDefault, enumType);
        enumType.Fields.Add(literal);
        literal.Constant = enumValue;
    }

    public bool Link(ModuleDefinition gameModule, ModuleDefinition modModule)
    {
        return true;
    }
}
