﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

public class TileEntityEditableStorageBox : TileEntitySecureLootContainer
{
    private string _boxText;
    private TextMesh[] _textMeshes;
    private bool isLocked;
    private string ownerID;
    private List<string> allowedUserIds;
    private string password;

    public TileEntityEditableStorageBox(Chunk _chunk) : base(_chunk)
    {
        allowedUserIds = new List<string>();
        isLocked = true;
        ownerID = "";
        password = "";
        _boxText = "Storage";
        SetText(_boxText, false);
    }

    //public bool IsLocked()
    //{
    //    return isLocked;
    //}

    //public void SetLocked(bool _isLocked)
    //{
    //    isLocked = _isLocked;
    //    setModified();
    //}

    //public void SetOwner(string _steamID)
    //{
    //    ownerID = _steamID;
    //    setModified();
    //}

    //public bool IsUserAllowed(string _steamID)
    //{
    //    return allowedUserIds.Contains(_steamID) || _steamID == ownerID;
    //}

    //public bool IsOwner(string _steamID)
    //{
    //    return _steamID == ownerID;
    //}

    //public string GetOwner()
    //{
    //    return ownerID;
    //}

    //public bool HasPassword()
    //{
    //    return !string.IsNullOrEmpty(password);
    //}

    //public string GetPassword()
    //{
    //    return password;
    //}

    //public List<string> GetUsers()
    //{
    //    return allowedUserIds;
    //}

    //public bool CheckPassword(string _password, string _steamID, out bool changed)
    //{
    //    changed = false;
    //    if (_steamID == ownerID)
    //    {
    //        if (Utils.HashString(_password) != password)
    //        {
    //            changed = true;
    //            password = Utils.HashString(_password);
    //            allowedUserIds.Clear();
    //            setModified();
    //        }
    //        return true;
    //    }
    //    if (Utils.HashString(_password) == password)
    //    {
    //        allowedUserIds.Add(_steamID);
    //        setModified();
    //        return true;
    //    }
    //    return false;
    //}

    public override void UpdateTick(World world)
    {
        base.UpdateTick(world);
        if (_textMeshes == null)
        {
            return;
        }

        if (_textMeshes.Any() && _boxText != _textMeshes.ElementAt(0).text)
        {
            SetText(_boxText, false);
        }
    }

    public override void read(BinaryReader _br, TileEntity.StreamModeRead _eStreamMode)
    {
        base.read(_br, _eStreamMode);
        _br.ReadInt32();
        isLocked = _br.ReadBoolean();
        ownerID = _br.ReadString();
        password = _br.ReadString();
        allowedUserIds = new List<string>();
        int num = _br.ReadInt32();
        for (int i = 0; i < num; i++)
        {
            allowedUserIds.Add(_br.ReadString());
        }
        _boxText = _br.ReadString();
        SetText(_boxText, false);
    }

    public override void write(BinaryWriter _bw, TileEntity.StreamModeWrite _eStreamMode)
    {
        base.write(_bw, _eStreamMode);
        _bw.Write(1);
        _bw.Write(isLocked);
        _bw.Write(ownerID);
        _bw.Write(password);
        _bw.Write(allowedUserIds.Count);
        for (int i = 0; i < allowedUserIds.Count; i++)
        {
            _bw.Write(allowedUserIds[i]);
        }
        _bw.Write(_boxText);

        SetText(_boxText, false);
    }

    //public override TileEntity Clone()
    //{
    //    return new TileEntityEditableStorageBox(chunk)
    //    {
    //        localChunkPos = base.localChunkPos,
    //        isLocked = this.isLocked,
    //        ownerID = this.ownerID,
    //        password = this.password,
    //        allowedUserIds = new List<string>(this.allowedUserIds),
    //        _boxText = this._boxText
    //    };
    //}

    public override void CopyFrom(TileEntity _other)
    {
        base.localChunkPos = ((TileEntityEditableStorageBox)_other).localChunkPos;
        this.isLocked = ((TileEntityEditableStorageBox)_other).isLocked;
        this.ownerID = ((TileEntityEditableStorageBox)_other).ownerID;
        this.password = ((TileEntityEditableStorageBox)_other).password;
        this.allowedUserIds = new List<string>(((TileEntityEditableStorageBox)_other).allowedUserIds);
        this._boxText = ((TileEntityEditableStorageBox)_other)._boxText;
    }

    //public int GetEntityID()
    //{
    //    return entityId;
    //}

    //public void SetEntityID(int _entityID)
    //{
    //    entityId = _entityID;
    //}

    public string GetText() { return _boxText; }

    public void SetText(string text, bool sync = true)
    {
        _boxText = text;
        if(_textMeshes != null && !GameManager.IsDedicatedServer)
        {
            foreach (var tMesh in _textMeshes)
            {
                tMesh.text = text;
            }
        }

        if (sync)
        {
            SetModified();
        }
    }

    public void SetBlockEntityData(BlockEntityData blockEntityData)
    {
        if (blockEntityData != null && blockEntityData.bHasTransform)
        {
            if (!GameManager.IsDedicatedServer)
            {
                _textMeshes = blockEntityData.transform.GetComponentsInChildren<TextMesh>();
            }
            SetText(_boxText, false);
        }
    }

    public override TileEntityType GetTileEntityType()
    {
        return TileEntityType.EditableStorageBox;
    }
}
